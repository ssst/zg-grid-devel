#!/bin/bash

export nodecfg_path="./${zgrid_bynodes_dir}"
export nodecfg_system_prefix="./zgrid-site"
export nodecfg_latstime_prefix="./not-in-vcs"

export nodecfg_grppath="./bygroups"

############################################

nodeconf_vars()
{
echo -n " " NODE_ID NODE_IDsuffix NODE_UUID NODE_HOST NODE_INSTPATH \
 NODE_GROUPS_append NODE_hostname NODE_hostid NODE_USER " "
}


nodeid_vars_all() # [API]
{
#nodeid_vars
nodeconf_vars
echo -n " "
hostid_vars_all
#hostinfo_vars
echo -n " "
#hook_nodeid_vars
main_call_hook nodeid_vars $*
}



# generic_loadconf_mode_exports=1|0
genericconf_load()
{
cfg=$1
varfunc=$2
if [ x"$generic_loadconf_mode_exports" == x0 ]; then
load_cfg_file_simple $cfg $varfunc
else
load_cfg_file_export $cfg NO_PREFIX NO_SUFFIX $varfunc
fi
}


load_cfg_file_export()
{
cfg=$1
varprefix=$2
varsuffix=$3
varfunc=$4

if [ -f $cfg ]; then
echo -n
else
return
fi
source $cfg

if [ $varsuffix == "NO_SUFFIX" ]; then
unset varsuffix
fi

if [ $varprefix == "NO_PREFIX" ]; then
unset varprefix
fi

for var in `$varfunc`; do
if [ x${!var} == x ]; then
echo -n
else
echo "export ${varprefix}${var}${varsuffix}=\"${!var}\" ;"
#echo
fi
done
}

load_cfg_file_simple()
{
cfg=$1
varfunc=$2

source $cfg
}


nodecfg_varid_from_nodeid()
{
varid=$1
varid=${varid/@/_0_}
varid=${varid/:/_1_}
echo $varid
}

nodecfg_nodeid_exists() # [API] [RECOMENDED]
{
#nodeconf_exists_nodeid $*
local nodeid cfg
nodeid=$1

if [ -z "$nodeid" ]; then 
dbg_echo nodecfg 1 "nodecfg_nodeid_exists : empty nodeid" 1>&2
return ;
fi

cfg=`nodecfg_nodeid_cfgfile $nodeid`

if [ -z "$cfg" ]; then
 return 1
fi

if [ -f $cfg ]; then
 echo -n
else
 return 1
fi
return 0
}

generic_trim()
{
echo $1
}

nodeconf_load()
{
# cmd : suffix|nosuffix
cmd=$1
nodeid=$2
varid=`nodecfg_varid_from_nodeid $nodeid`
varsuffix=_$varid
pushd $ZGRIDBASEDIR > /dev/null

if [ $cmd == "suffix" ]; then
echo "export NODE_ok_${varid}=1 ;"
fi
if [ $cmd == "SUFFIX" ]; then
echo "export NODE_ok_${varid}=1 ;"
fi

# check if any node like this
nodecfg_nodeid_exists $nodeid
if [  $? == 0 ]; then
 dbg_echo nodecfg 2 "nodeconf_load : ok, node $nodeid found" 1>&2
else
 echo "ERROR: no such node \"$nodeid\""
 exit
fi

unset `nodeconf_vars`

#NODE_GROUPS_all=","

_nodeid_cfgfile=`nodecfg_nodeid_cfgfile ${nodeid}`
_nodeid_cfgdir=`nodecfg_nodeid_cfgdir ${nodeid}`

##### ADDITIONAL CFG #######

#_path=${nodecfg_path}/${nodeid}/etc/nodeconf
_path=${_nodeid_cfgdir}/etc/nodeconf

if [ -d ${_path} ]; then

 LISTFILES=`find ${_path}  -type f`
 for cfg in $LISTFILES; do
 #echo echo $cfg ";"
 varX=`load_cfg_file_export $cfg NO_PREFIX NO_SUFFIX nodeconf_vars`
 #echo $varX;
 eval $varX;
 NODE_GROUPS_all="$NODE_GROUPS_all $NODE_GROUPS_append"
 #unset `nodeconf_vars`
 unset NODE_GROUPS_append
 done

fi

####### MAIN CFG ###########

##echo echo $cfg ";"
#cfg=${nodecfg_path}/${nodeid}/${nodeid}.nodeconf
#var2=`load_cfg_file_export $cfg NO_PREFIX NO_SUFFIX nodeconf_vars`
#echo $var2;
#eval $var2;
#NODE_GROUPS_all="$NODE_GROUPS_all $NODE_GROUPS_append"
##unset `nodeconf_vars`
#unset NODE_GROUPS_append


#cfg=${nodecfg_path}/${nodeid}/this.nodeconf
cfg=${_nodeid_cfgfile}

#echo echo $cfg ";"
#load_cfg_file_export $cfg NONE nodeconf_vars
var1=`load_cfg_file_export $cfg NO_PREFIX  NO_SUFFIX nodeconf_vars`
#echo $var1;
eval $var1;
NODE_GROUPS_all="$NODE_GROUPS_all,$NODE_GROUPS_append"
#unset `nodeconf_vars`
unset NODE_GROUPS_append

#### groups #####
NODE_GROUPS_all=${NODE_GROUPS_all//,/ }
dbg_echo nodecfg 3 [2] NODE_GROUPS_all=$NODE_GROUPS_all 1>&2
NODE_GROUPS_append=$NODE_GROUPS_all

export NODE_GROUPS_dirs

for grp in $NODE_GROUPS_all; do
cfg=${nodecfg_grppath}/${grp}/;
#echo echo $cfg ";"
if [ -f ${cfg}/this.groupconf ]; then 
dbg_echo nodecfg 3 [2] "$cfg : ok, this dir exists" 1>&2
NODE_GROUPS_dirs="$NODE_GROUPS_dirs ${cfg}"
fi
done

###### output  #####

for var in `nodeconf_vars` NODE_GROUPS_dirs ; do
if [ x"${!var}" == x ]; then
echo -n
else

#var='${var#"${var%%[![:space:]]*}"}'
#var_value=`trim ${!var}`

# trim leading & ... spaces
res="${!var}"; res=`generic_trim "$res"` #echo "res=$res" 1>&2
read -r $var <<< "$res"

if [ $cmd == "suffix" ]; then
echo "export ${varprefix}${var}${varsuffix}=\"${!var}\" ;"
else
echo "export ${varprefix}${var}=\"${!var}\" ;"
fi

#echo
fi
done
popd $ZGRIDBASEDIR > /dev/null
}

############################

cfgstack_seach_dir_list()
{
#cmd=$1
#cfgprefix=$2
#cfgfile=$3
#nodeid=$4

LIST=""
LIST="${nodecfg_system_prefix}"
# 


#varid=`nodecfg_varid_from_nodeid $nodeid`
dbg_echo cfgstack 5 [2] cfgstack_seach_dir_list nodeid=$nodeid 1>&2
#nodeconf_load nosuffix $nodeid
#var=`nodeconf_load nosuffix $nodeid`;eval $var;unset var
#echo var=$var 1>&2
nodeconf_load nosuffix $nodeid 1> /dev/null

#var=`hostinfo_loadconf_byid $NODE_HOST`;eval $var;unset var
#echo  [2] NODE_HOST=$NODE_HOST 1>&2
#hostinfo_loadconf_byid
LIST="$LIST ${nodecfg_path}/$NODE_HOST"

#listvar="NODE_GROUPS_append_$varid"
dbg_echo cfgstack 5  [2] NODE_GROUPS_append= $NODE_GROUPS_append 1>&2
dbg_echo cfgstack 5  [2] NODE_GROUPS_dirs= $NODE_GROUPS_dirs 1>&2
LIST="$LIST $NODE_GROUPS_dirs"

LIST="$LIST ${nodecfg_path}/${nodeid}"

LIST="$LIST ${nodecfg_latstime_prefix}"
#CFGSTACK_search_dir_list="${LIST}"
echo "export CFGSTACK_search_dir_list=\"${LIST}\""
}


cfgstack_cfg()
{
# cmd : trace exports load listfiles
local cmd cfgprefix cfgfile nodeid

cmd=$1
cfgprefix=$2
cfgfile=$3
nodeid=$4

pushd $ZGRIDBASEDIR > /dev/null

nodecfg_nodeid_exists $nodeid

if [  $? == 0 ]; then
 dbg_echo nodecfg 1 "cfgstack_cfg : ok, node nodeid=$nodeid found" 1>&2
else
 popd > /dev/null
 echo "cfgstack_cfg: ERROR: no such node '$nodeid'"
 return
 #exit
fi



var=`cfgstack_seach_dir_list`
eval $var;unset var;

dbg_echo cfgstack 5 [2] "dir list to check=$CFGSTACK_search_dir_list" 1>&2 
cfgstack_msg_trace $cmd 
for _dir in $CFGSTACK_search_dir_list; do
#cfgstack_msg_trace $cmd 
cfgstack_msg_trace $cmd -n "Check config : "
cfg=${_dir}/$cfgfile
#echo cfgstack_cfg_one $cmd $cfgprefix $cfg 
cfgstack_cfg_one $cmd $cfgprefix $cfg 

done
cfgstack_msg_trace $cmd 
popd > /dev/null

}

cfgstack_load_simple() # [API] [RECOMENDED]
{
local cmd cfgfile
cfgfile=$1
cfgstack_cfg load UNKNOWN $cfgfile $THIS_NODEID
}

#############################

cfgstack_load_byid() # [API] [RECOMENDED]
{
local cmd cfgfile _nodeid
cfgfile=$1
_nodeid=$2
cfgstack_cfg_v2 load $cfgfile ${_nodeid}
}

nodecfg_id_type() # [API] [RECOMENDED]
{
nodecfg_nodeid_exists
if [  $? == 0 ]; then echo "nodeid";return 0; fi
nodecfg_hostid_exists
if [  $? == 0 ]; then echo "hostid";return 0; fi
echo "notexist";return 1
echo -n
}


cfgstack_cfg_v2()
{
# cmd : trace exports load listfiles
local cmd cfgprefix cfgfile nodeid

cmd=$1
cfgfile=$2
nodeid=$3
cfgprefix="UNKNOWN"

pushd $ZGRIDBASEDIR > /dev/null

nodecfg_id_type $nodeid > /dev/null

if [  $? == 0 ]; then
echo -n # dbg_echo nodecfg 1 "cfgstack_cfg : ok, node/host/... id=$nodeid found" 1>&2
else
 popd > /dev/null
 echo "cfgstack_cfg: ERROR: no such node/host/... '$nodeid'"
 return
 #exit
fi



var=`cfgstack_seach_dir_list`
eval $var;unset var;

dbg_echo cfgstack 5 [2] "dir list to check=$CFGSTACK_search_dir_list" 1>&2 
cfgstack_msg_trace $cmd 
for _dir in $CFGSTACK_search_dir_list; do
#cfgstack_msg_trace $cmd 
cfgstack_msg_trace $cmd -n "Check config : "
cfg=${_dir}/$cfgfile
#echo cfgstack_cfg_one $cmd $cfgprefix $cfg 
cfgstack_cfg_one $cmd $cfgprefix $cfg 

done
cfgstack_msg_trace $cmd 
popd > /dev/null

}




#############################


cfgstack_loadshellcfg()
{
cfgprefix=$1
cfg=$2

source $cfg
}


cfgstack_load_exports()
{
cfgprefix=$1
cfg=$2
cat $cfg|grep "^$cfgprefix"
}

cfgstack_msg_trace()
{
cmd=$1
shift 1
if [ $cmd == "trace" ]; then
echo $*
fi
}

cfgstack_msgprintf_trace()
{
cmd=$1
shift 1
if [ $cmd == "trace" ]; then
printf $*
fi
}


cfgstack_cfg_one()
{
cmd=$1
cfgprefix=$2
cfg=$3


if [ -f $cfg ]; then
cfgstack_msgprintf_trace $cmd "%8s%2s" "[FOUND] "
else
cfgstack_msgprintf_trace $cmd "%8s%2s" "[_not_] "
fi

cfgstack_msg_trace $cmd "$cfg  ."

if [ -f $cfg ]; then
# load config if needed
if [ x$cmd == x"load" ]; then
dbg_echo cfgstack 1  [2] "cfgstack_cfg_one(): cfgstack_loadshellcfg $cfgprefix $cfg"  1>&2
cfgstack_loadshellcfg $cfgprefix $cfg
fi

if [ x$cmd == x"exports" ]; then
dbg_echo cfgstack 1  [2] "cfgstack_cfg_one():cfgstack_load_exports $cfgprefix $cfg"  1>&2
cfgstack_load_exports $cfgprefix $cfg
fi

return 0
else
return 1
fi
}

cfgstack_cfg_thisnode() # [API] [RECOMENDED]
{
local _cfgfile=$1

if [ x"$THIS_NODEID" == "x" ]; then
echo "THIS_NODE not defined"
else
#cfgstack_cfg $cmd $cfgfile $THIS_NODEID
cfgstack_load_byid ${_cfgfile} ${THIS_NODEID}
fi
}


nodecfg_nodedir_list_set()
{
var=`find $nodecfg_path -iname "*.nodeconf" | xargs --no-run-if-empty -n 1 dirname `
echo "export NODECFG_nodedir_list=\"$var\" ; "
export NODECFG_nodedir_list="$var"
nodecfg_vars_list_set
}

nodecfg_vars_list_set()
{
local _id var cfg cfg1 varid
for d in $NODECFG_nodedir_list ; do
_id=`basename $d`
varid=`nodecfg_varid_from_nodeid ${_id}`
var="NODECFG_cfgdir_$varid"
echo "export $var=\"$d\" ; "

unset cfg

cfg1="$d/this.nodeconf"
if [ -f $cfg1  ]; then
cfg=$cfg1
fi

cfg1="$d/${_id}.nodeconf"
if [ -f $cfg1  ]; then
cfg=$cfg1
fi

#echo "echo "
if [ -n "$cfg" -a -n "$varid"  ]; then
var="NODECFG_cfgfile_$varid"
echo "export $var=\"$cfg\" ; "
fi

unset var
unset cfg
done
}
# 
nodecfg_nodeid_cfgfile() # [API]
{
local _id var cfg cfg1
_id=$1
var=`nodecfg_varid_from_nodeid ${_id}`

var=NODECFG_cfgfile_$var

echo -n ${!var}
}
nodecfg_nodeid_cfgdir() # [API]
{
local _id var cfg cfg1
_id=$1
var=`nodecfg_varid_from_nodeid ${_id}`

var=NODECFG_cfgdir_$var

echo -n ${!var}
}



nodecfg_nodeid_load() # [API] [RECOMENDED]
{
local cfg varX nodeid

nodeid=$1

if [ x"$nodeid" == x ]; then
echo "[2] msg: nodecfg_nodeid_load(): nodeid is empty" 1>&2
return 1
fi

# next: use variables with cache

unset `nodeid_vars_all`

# chack if config present
cfg=`nodecfg_nodeid_cfgfile ${nodeid}`
if [ x"$cfg" == x"" ]; then
echo "[2] msg: nodecfg_nodeid_load(): nodeid=$nodeid not exists" 1>&2
return 1
fi

main_call_hook nodeid_before_load_api $*

varX=`nodeconf_load NOSUFFIX $nodeid`
eval $varX;unset varX

#echo "nodeid=$NODE_ID"
dbg_echo nodecfg 3 "$NODE_ID node=$NODE_HOST" 1>&2 

export hostinfo_loadconf_mode_exports=1
varX=`hostcfg_hostid_load $NODE_HOST`
eval $varX;

#hook_nodeid_load_api
main_call_hook nodeid_load_api $*
main_call_hook nodeid_after_load_api $*

}


nodecfg_nodeid_load_api()
{
#echo nid=$NODE_ID

if [ x$NODE_USER ]; then

while IFS='@' read -ra ARRAY; do
export NODE_USER=${ARRAY[0]}
done <<< "$NODE_ID"
#echo NODE_USER=$NODE_USER
export NODE_USER=$NODE_USER
fi

}




nodecfg_iterate_full_nodeid() # [API] [RECOMENDED]
{
func=$1

pushd $ZGRIDBASEDIR >  /dev/null

_nodeconf_vars_list=`nodeid_vars_all`


unset ${_nodeconf_vars_list}
for _dir in $NODECFG_nodedir_list; do

_id=`basename ${_dir}`

nodecfg_nodeid_load $_id

dbg_echo nodecfg 3 "$NODE_ID node=$NODE_HOST" 1>&2 

# DEVELMARK
#echo "HOST_id $HOST_id" 2>&1

eval $func

unset ${_nodeconf_vars_list}
done

popd >  /dev/null
}



nodecfg_iterate_simple_nodeid()
{
func=$1

pushd $ZGRIDBASEDIR >  /dev/null

_nodeconf_vars_list="`nodeconf_vars` `hostfullconfig_vars_all`"
unset ${_nodeconf_vars_list}

for _dir in $NODECFG_nodedir_list; do
cfg=${_dir}/this.nodeconf
varX=`load_cfg_file_export $cfg NO_PREFIX NO_SUFFIX nodeconf_vars`
eval $varX;

hostcfg_hostid_load $NODE_HOST > /dev/null

eval $func

unset ${_nodeconf_vars_list}
done

#echo nodecfg_iterate_simple_nodeid vars=${_nodeconf_vars_list} 1>&2
popd >  /dev/null

}

_this_nodeid_detect_hlpr()
{
HOSTNAME=`hostname`
local check_hostname

dbg_echo this 3 [2]  NODE_INSTPATH=$NODE_INSTPATH  ZGRIDBASEDIR=$ZGRIDBASEDIR   1>&2
dbg_echo this 3 [2] NODE_hostname=\"$NODE_hostname\" HOSTNAME=\"$HOSTNAME\" 1>&2
dbg_echo this 3 [2] NODE_ID=$NODE_ID 1>&2

check_hostname=$NODE_hostname
if [ x"$check_hostname" == x ]; then
check_hostname=$HOST_hostname
fi


if [ x"$NODE_INSTPATH" == x"$ZGRIDBASEDIR" ]; then
if [ x"$check_hostname" == x"$HOSTNAME" ]; then
dbg_echo this 2 [2] "THIS_NODEID=$NODE_ID"  1>&2
echo "export THIS_NODEID=$NODE_ID"
return 0
fi
fi
return 1
}

this_nodeid_detect()
{
if [ -f ./not-in-vcs/etc/this-install.conf ]; then
source ./not-in-vcs/etc/this-install.conf
if [ x"$THIS_NODEID" == x ]; then
echo -n
else
echo "export THIS_NODEID=$THIS_NODEID"
return
fi
fi

var=`nodecfg_nodedir_list_set`
eval $var; unset var

#echo [2] NODECFG_nodedir_list=$NODECFG_nodedir_list 1>&2
#echo [2] ZGRIDBASEDIR=$ZGRIDBASEDIR 1>&2
#nodeid=$USER@$HOSTNAME:$ZGRIDBASEDIR
#echo [2] nodeid=$nodeid 1>&2

nodecfg_iterate_simple_nodeid _this_nodeid_detect_hlpr

}


nodecfg_envset_prestart()
{
nodecfg_nodedir_list_set
dbg_echo nodecfg 2 [2] "nodecfg_envset_prestart(): START"  1>&2
#this_nodeid_detect
cache_wrap_func this_nodeid_detect this_nodeid_detect
dbg_echo nodecfg 2 [2] "nodecfg_envset_prestart(): END"  1>&2

}



#source ${MODINFO_modpath_nodecfg}/nodecfg_hst.bash
source ${MODINFO_modpath_nodecfg}/hostcfg.bash

