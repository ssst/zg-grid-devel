
#############  bootstrap file for shell scripts -- 1st stage  #########################

# loading of zgrid main config

if [ -f "./zgrid-site/etc/zgrid.conf" ]; then
echo -n
else
echo -n " ./zgrid-site/etc/zgrid.conf not found : "
echo "zgrid not initialized, aborting"
exit
fi

source ./zgrid/main/zgrid-structure.conf

source ./zgrid-site/etc/zgrid.conf

if [ -f "./zgrid-site/etc/cache-zgrid.conf" ]; then
source "./zgrid-site/etc/cache-zgrid.conf"
fi

#

export ZGRID_gridname
export GRIDBASEDIR=`pwd`
export ZGRIDBASEDIR=`pwd`

if [ -d ./zgrid ]; then
echo -n
else
echo "ABORT! [BUG] we are not in zgrid install dir!"
exit
fi

############

unset d

############

# main module
. ./zgrid/main/lib.sh
. ./zgrid/main/main.bash

############
# cache
source ./zgrid/main/cache/cache.inc.sh

var=`cache_key_set`
eval ${var} ; unset var

var=`cache_path_set`
#echo [2] var=$var 1>&2
eval $var; unset var


#echo [2] cache_key=$cache_key 1>&2
dbg_echo cache 3 [2] cache_path=$cache_path 1>&2

# end cache init
############


# MODINFO_files
#export MODINFO_files="`main_list_modinfo` F___NGHACK"
export MODINFO_files="`main_list_modinfo`"

. ./zgrid/main/lib2.sh

#for i in $MODINFO_files; do
#echo $i
#done

#. ./zgrid/main/nodecfg.bash
#. ./zgrid/main/loadmodules.sh



#############  bootstrap file for shell scripts  - 2ns stage  ##################

#main_mod_runfunc : $name $mod_dir
#var=`main_mod_runfunc main_load_mod_functions`
#eval $var;unset var;

#main_mod_runfunc_fast_enable
var=`main_mod_runfunc_fast_enable main_load_mod_functions_fast`
eval $var;unset var;


################################################################################

# call hooks before anything happened in CLI/script/whatever

# hook for var set
var=`main_envset_prestart_hook`
eval $var;
dbg_echo_var_stderr loadconfigs 4 $var ;
unset var;
# for calls
main_env_prestart_hook

# main hook for var set
var=`main_envset_start_hook`
eval $var;
dbg_echo_var_stderr loadconfigs 4 $var ;
unset var;
# main hook
main_env_start_hook

# hook for var set
var=`main_envset_poststart_hook`
eval $var;dbg_echo_var_stderr loadconfigs 4 $var ;
unset var;
# for fixes
main_env_poststart_hook



