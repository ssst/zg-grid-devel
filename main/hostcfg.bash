#!/bin/bash

#############################################

hostinfo_vars()
{
echo -n HST HSTNAME SCANHST DNSNAMES HOST_NAME \
HOST_id HOST_hostid HOST_hostname  HOST_dnsname HOST_SCANHST HOST_uuid  " "
}

hostid_vars_all() # [API]
{
#hostfullconfig_vars
hostinfo_vars
echo -n " "
main_call_hook hostfullconfig_vars $*
main_call_hook hostid_vars $*
}

hostfullconfig_vars_all()
{
hostid_vars_all
}



hostinfo_loadconf()
{
#hostinfo_loadconf_mode_exports
#if [ x$hostinfo_loadconf_mode_exports == x0 ]; then
#fi
# generic_loadconf_mode_exports=1|0

generic_loadconf_mode_exports=0
genericconf_load $1 hostinfo_vars

export `hostinfo_vars`
}

hostcfg_hostid_cfgfile() # [API] [RECOMENDED]
{
local _hostid=$1
echo -n ${nodecfg_path}/${_hostid}/${_hostid}.hostinfo
}

hostcfg_hostid_cfgdir() # [API] [RECOMENDED]
{
local _hostid=$1
echo -n ${nodecfg_path}/${_hostid}
}

hostcfg_hostid_load() # [API] [RECOMENDED]
{
local _hostid cfgfile varX i var hostid_vars_list
_hostid=$1

if [ x"${_hostid}" == "x" ]; then 
dbg_echo hostcfg 1  "hostcfg_hostid_load() UNKNOWN _hostid == ''" 1>&2
return
fi

main_call_hook hostid_pre_load_api ${_hostid}

hostid_vars_list=`hostid_vars_all`

unset $hostid_vars_list

cfgfile=`hostcfg_hostid_cfgfile ${_hostid}`
dbg_echo hostcfg 3 "hostcfg_hostid_load() {_hostid}=${_hostid}  cfgfile=$cfgfile " 1>&2
hostinfo_loadconf $cfgfile

main_call_hook hostid_load_api ${_hostid}
main_call_hook hostid_post_load_api ${_hostid}

for i in $hostid_vars_list ; do
echo -n "export $i=${!i} ; "
done

}



function hostcfg_iterate_hostid # [API] [RECOMENDED]
{
local hostinfo_vars_list code F varX

code=$1
pushd $GRIDBASEDIR > /dev/null


hostinfo_vars_list=`hostid_vars_all`
dbg_echo nodecfg 2 hostcfg_iterate_hostid [2] hostinfo_vars_list=$hostinfo_vars_list 1>&2 ;

find $nodecfg_path -iname "*.hostinfo"  | while read F; do
unset $hostinfo_vars_list

hostinfo_loadconf $F

varX=`hostcfg_hostid_load $HOST_id`
eval $varX;unset varX

dbg_echo hostcfg 1 hostcfg_iterate_hostid [2] incoming_scanhst=$incoming_scanhst 1>&2 ;
dbg_echo hostcfg 1 hostcfg_iterate_hostid [2] incoming_detect_type=$incoming_detect_type

_dir=`dirname $F`
export cfgfile_dir=$_dir
eval "$code"

unset $hostinfo_vars_list
done

popd > /dev/null
}


############################################################

# adding and etc

nodecfg_add_this_node()
{
#nodemainname=$1
#nodeid=${USER}@${nodemainname}

HSTNAME=`hostname`
HST=`hostname`
NODEPATH=`pwd`

nodecfg_add_nodecfg $USER $HSTNAME $NODEPATH
}

#nodecfg_load_

nodecfg_add_nodecfg()
{
nodeuser=$1
nodehost=$2
nodepath=$3
nodeid=${USER}@${nodehost}

echo "create nodeid=$nodeid"

NODEDIR="./bynodes/$nodeid/"

mkdir -p $NODEDIR
touch ${NODEDIR}/.keep_me

}
nodecfg_add_hostcfg()
{
paramfunc=$1
#nodehost=$2
#nodehostname=$2
#HOST_NAME=$1
#HOST_hostname=$2
# add node host

var=`$paramfunc` ; eval $var ; unset var

NODEHOST_DIR="./bynodes/$HOST_NAME/"
cfg=${NODEHOST_DIR}/$HOST_NAME.hostinfo

#dbg_echo 5 
echo nodecfg [2] nodecfg_add_hostcfg HOST_NAME=$HOST_NAME  1>&2 ;

#exit

if [ -f $NODEHOST_DIR ]; then
 if [ -d $NODEHOST_DIR ]; then
 echo -n
 else
 echo "$NODEHOST_DIR existed and it is not directory!"
 return
 fi
fi

if [ -d $NODEHOST_DIR ]; then
echo "host dir already exited"
return
else
echo "Creating $NODEHOST_DIR for $HOST_NAME"
mkdir -p ${NODEHOST_DIR}
touch ${NODEHOST_DIR}/.keep_me
fi

echo "[2] hostinfo_vars=`hostinfo_vars`" 2>&1 

for var in `hostinfo_vars`; do
if [ x"${!var}" == "x" ]; then
echo "#$var=\"\"" >> $cfg
else
echo "$var=\"${!var}\"" >> $cfg
fi
done
echo "" >> $cfg
}

hostcfg_hostid_exists() # [API] [RECOMENDED]
{
#nodeconf_exists_nodeid $*
local hostid cfg
hostid=$1

if [ -z "$hostid" ]; then echo "hostcfg_hostid_exists : empty nodeid";
return ; fi

cfg=`hostcfg_hostid_cfgfile $hostid`

if [ -z "$cfg" ]; then
 return 1
fi

if [ -f $cfg ]; then
 echo -n
else
 return 1
fi
return 0
}



