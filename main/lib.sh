#!/bin/bash

#main_checkfunc()
#{
#cdir=`pwd`
#if [ $cdir != ]; then 
#fi
#}

export main_modpath="./zgrid/ ./zgrid-site/modules"
export main_cfg_modpath="zgrid-site/etc/modules"


##############################################

# debug functions

dbg_echo()
{
local mod this_msg_lvl var

mod=$1
this_msg_lvl=$2
shift 2
var=MODINFO_dbg_$mod
#echo "$var = ${!var}"
if [ x${!var} == "x" ]; then
#echo "no debug"
echo -n
else
if [ ${!var} -ge $this_msg_lvl ]; then
echo $*
fi
fi
unset mod
}

dbg_echo_var_stderr()
{
local mod this_msg_lvl var

mod=$1
this_msg_lvl=$2
shift 2
var=$*

dbg_echo $mod $this_msg_lvl $mod $this_msg_lvl "LOADCONF ##### var #####"  1>&2
#echo ${!var}  1>&2
dbg_echo $mod $this_msg_lvl $mod $this_msg_lvl ${var}  1>&2
dbg_echo $mod $this_msg_lvl $mod $this_msg_lvl "LOADCONF #####  #####"  1>&2
}

##############################################

zgrid_lib_vars()
{
echo -n ZGRIDLIB_version ZGRIDLIB_thisclidir
}

zgrid_vars()
{
echo -n ZGRIDBASEDIR GRIDBASEDIR ZGRID_GRIDNAME zgrid_core \
zgrid_core_version  zgrid_core_version_major zgrid_core_version_minor
}

zgrid_fullenv_vars()
{
zgrid_lib_vars zgrid_vars
}

##################################

_generic_listvar_x1()
{
var=$1
shift 1
unset f;
if [ $var == "'" ]; then
f=0
fi

for i in $* ; do
if [ $var == $i ]; then
f=0
fi
done

if [ x$f == x0 ]; then
echo -n
else
echo -n $var; echo -n " "
fi
}

generic_listvar_except()
{
pat="=*"
( set -o posix ; set )|while read F; do
 var=${F%%$pat}
 #echo "#$var#" 1>&2
 _generic_listvar_x1 ${var} pat $*
done
unset pat
}


##################################

modinfo_vars()
{
echo name version description group modtypes
}

modinfo_loadconf()
{
local f=$1
unset `modinfo_vars`

if [ -f $f ]; then echo -n ; else dbg_echo main 6 "modinfo_loadconf(): not found \"$f\"";return 1; fi
dbg_echo main 6 "modinfo_loadconf(): load $f"
source $1
if [ x${group} == x"" ]; then group="other";fi
}


###################################

loadshellcfg()
{
# MUST ADD CHECKS!
source $1
}

main_list_modinfo()
{
local list var vars _file _dir path

# MODINFO_files
list=`find $main_modpath -iname "*.modinfo" -printf " %p "`
for path in $list; do
 _file=`basename $path`
 _dir=`dirname $path`
 _file=${_file%%.modinfo}
 var=tmp_${_file}
 #local $var
 vars="$vars $var"
 read -r $var <<< $path
 #echo $var 1>&2
done
#echo $vars 1>&2
for var in $vars; do 
if [ x${!var} == x ]; then
echo -n
else
echo -n ${!var}" "; 
unset $var
fi
done
}

modinfo_header_var1()
{
local F _dir varname

pushd $GRIDBASEDIR > /dev/null
#echo $MODINFO_files  | while read -d\  F; do
for F in $MODINFO_files ; do
#echo echo F=$F #exit
unset `modinfo_vars`
modinfo_loadconf $F
varname="MODINFO_modpath_${name}"
# http://mywiki.wooledge.org/BashFAQ/006 :
# In Bash, we can use read and Bash's here string syntax: 
#IFS= read -r $varname <<< "$F"
read -r $varname <<< "$F"
_dir=`dirname $F`

if [ x"$name" == "x" ]; then echo -n; else
#echo "[2] export $varname=\"${_dir}\"  " 1>&2
echo "export $varname=\"${_dir}\" ; "
modinfo_etc_outvar $name
#m_all="$m_all $name"
fi

unset `modinfo_vars`
done
#echo "---------------------- " 1>&2
#echo "[2] m_all=$m_all  " 1>&2

popd > /dev/null

}

modinfo_etc_outvar()
{
pushd $GRIDBASEDIR > /dev/null

name=$1
unset `modinfo_vars2`
modinfo_etcconf $name
varname_enable="MODINFO_enable_${name}"
# http://mywiki.wooledge.org/BashFAQ/006 :
# In Bash, we can use read and Bash's here string syntax: 
#IFS= read -r $varname <<< "$F"
read -r $varname_enable <<< "$MOD_enable"
if [ x"$MOD_enable" == "x" ]; then echo -n; else
echo "export ${varname_enable}=\"$MOD_enable\" ; "
fi
unset `modinfo_vars2`
popd > /dev/null

}


modinfo_setvar1_snippet()
{

##
varname_enable="MODINFO_enable_${name}"
if [ x${!varname_enable} == xY ]; then
#echo "dbg: mods_enabled=$name $mods_enabled"
mods_enabled="$name $mods_enabled"
#echo "export mods_enabled=\"$mods_enabled\";"
else
mods_disabled="$name $mods_disabled"
#echo "export mods_disabled=\"$mods_disabled\";"
fi
mods_all="$name $mods_all"
#echo "export mods_all=\"$mods_all\";"
#echo "mods_enabled=\"$mods_enabled\";" 1>&2

##
}

modinfo_header_module_list()
{
#export mods_enabled mods_disabled mods_all

pushd $GRIDBASEDIR > /dev/null
main_mod_runfunc 'modinfo_setvar1_snippet' > /dev/null
popd > /dev/null

dbg_echo "modinfo" 4 "[2] mods_enabled=\"${mods_enabled}\";" 1>&2

echo "export MODULE_list_enabled=\"$mods_enabled\"; "
echo "export MODULE_list_disabled=\"$mods_disabled\"; "
echo "export MODULE_list_all=\"$mods_all\"; "

#echo "export MODULE_list_enabled=\"\$mods_enabled\";unset mods_enabled ;"
#echo "export MODULE_list_disabled=\"\$mods_disabled\";unset mods_disabled ;"
#echo "export MODULE_list_all=\"\$mods_all\";unset mods_all ;"
}


modinfo_vars2()
{
echo MOD_name MOD_enable MOD_version MOD_path MOD_version_stamp
}

main_mod_cfg_vars()
{
modinfo_vars2
}


loadshellcfg()
{
# MUST ADD CHECKS!
source $1
}

modinfo_etcconf()
{
name=$1
cfg=$main_cfg_modpath/${name}.modconfig
if [ -f $cfg ]; then
#echo "bbb=$cfg"
loadshellcfg $cfg
else
echo -n
fi
}



function main_mod_runfunc
{
local code F _dir
code=$1
pushd $GRIDBASEDIR > /dev/null
dbg_echo main 6 "main_mod_runfunc() -------------- "  1>&2
dbg_echo main 6 GRIDBASEDIR=$GRIDBASEDIR  1>&2

#find $main_modpath -iname "*.modinfo"  | while read F;
#echo $MODINFO_files  | while read -d\  F; do
for F in $MODINFO_files ; do
unset `modinfo_vars`
dbg_echo main 6 "main_mod_runfunc() last pwd="`pwd`  1>&2
cd $GRIDBASEDIR
dbg_echo main 6 "main_mod_runfunc() set pwd="`pwd`  1>&2
modinfo_loadconf $F  1>&2
_dir=`dirname $F`
export mod_dir=${_dir}
dbg_echo main 6 "F=$F name=$name description=$description depend=$depend group=$group version=$version "  1>&2
#eval "( cd $GRIDBASEDIR ; $code )"
cd $GRIDBASEDIR
eval "$code"
cd $GRIDBASEDIR
done

popd > /dev/null
}

function main_mod_runfunc_fast_enable
{
local i var code
code=$*
for i in $MODULE_list_enabled; do

#if [ $i == "main" -o $i == "nodecfg" ]; then 
#echo -n
#else
var=MODINFO_modpath_$i
#echo $var=${!var} 1>&2
name=$i
mod_dir=${!var}
eval "$code"
unset mod_dir
#fi
done
}

###############################################

# loading .bash modules

main_load_mod_functions()
{
#name=$1;
#moddir=$2;
s="$mod_dir/${name}.bash"
if [ -f $s ]; then

modvar1="MODINFO_enable_${name}"
if [ x${!modvar1} == x"Y" ]; then
dbg_echo main 3 "load $s, enabled" 1>&2;
echo "source $s ;"
else
dbg_echo main 3 "not load $s, not enabled" 1>&2;
echo -n
fi

else
dbg_echo main 3 "$name : .bash not found" 1>&2;
fi
}

main_load_mod_functions_fast()
{
dbg_echo main 3 source ${mod_dir}/${name}.bash 1>&2
echo "source ${mod_dir}/${name}.bash ;"
}

###############################################

main_modules_always_enabled()
{
export MODINFO_enable_main=Y
export MODINFO_enable_nodecfg=Y
export MODINFO_enable_system=Y
#export MODINFO_enable_hoststat=Y
}

