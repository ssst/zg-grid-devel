#!/bin/bash

################## zgrid header1 ################ 
if [ x$ORIGDIR == x ]; then
export ORIGDIR=`pwd`
fi

_file0=`readlink -f $0`
_dir0=`dirname $_file0`
cd ${_dir0}

. ./libzgrid.sh
################## [END] zgrid header1 ################ 

if [ "x${RUN_FROM_init_zgrid_structure}" == "x" ]; then
echo "support script"
#exit
fi

echo "INIT SECOND STAGE"
. ./zgrid/init/initgrid.bash

echo_log()
{
echo $*
echo $*  >> $GRIDBASEDIR/not-in-vcs/install.log
}

#main_getmodlist 
run_mod_init()
{
echo "run_mod_init(): $*"

name=$1;
moddir=$2;
stage=$3;
s="$moddir/${name}.initgrid"
#cd $ZGRIDBASEDIR
dbg_echo initgrid 6 "run_mod_init() pwd="`pwd`
dbg_echo initgrid 6 "run_mod_init() check path: $s"
if [ -f $s ]; then
echo -n
dbg_echo initgrid 4 "run $s $stage"
source $s $stage
else
echo "$name : .initgrid not found, ok search next"
fi
}

initgrid_zgridsys()
{
echo "zgridsys $*"
system_f_cleanenv  ./zgrid/modules/zgridsys/zgridsys $*
#zgridsys_cli_main $*
}

run(){

echo "Run all .initgrid scripts from all avaliable modules."
main_mod_runfunc 'run_mod_init $name $mod_dir stage1'


# fix some 1st
mkdir_init ./bynodes/
touch ./bynodes/.keep_me

# enable main CLI utility
MODINFO_dbg_main=10
echo "== enable main CLI utility: ./zgrid/main/modules-enable zgridsys"
#system_f_cleanenv ./zgrid/main/modules-enable zgridsys
./zgrid/main/modules-enable zgridsys
echo "== END (enable main CLI utility: ./zgrid/main/modules-enable zgridsys)"


# system module funcs load (manual)
#main_load_module_into_context system # this not works
#print_vars
#exit
echo "main_load_module_into_context zgridsys :"
main_load_module_into_context zgridsys


# clear cache
initgrid_zgridsys module cache_clear
#./zgrid/main/modules-cache-clear

#echo -n "MODINFO: ";( set -o posix ; set ) |grep -i modinfo | wc -l

echo "add files from enabled zgridsys"
cp -v ./zgrid/modules/zgridsys/zgridsys ./zgrid-site/bin/${ZGRID_zgridname}-zgridsys
cp -v ./zgrid/modules/zgridsys/libzgrid.sh ./zgrid-site/bin/

# install CLI in ~/bin/ directory
if [ -f $HOME/bin/${ZGRID_zgridname}-zgridsys ]; then
echo "ok, \$HOME/bin/${ZGRID_zgridname}-zgridsys already installed"
else
ln -s $ZGRIDBASEDIR/zgrid-site/bin/${ZGRID_zgridname}-zgridsys $HOME/bin/
fi



# add this node
#nodecfg_add_this_node
mkdir -p ./not-in-vcs/attach/
initgrid_zgridsys nodecfg addthis

mkdir_init ./bynodes/
cp -v -r ./not-in-vcs/attach//thisnode/cfg/* ./bynodes/

#init dvcs (hg)
hgone_initthisnodestorage_initgrid
hgone_register_all_changes `system_trans_genid`

# clear cache, after finish
initgrid_zgridsys module cache_clear

# we should be ok now to run module supplied scripts
#MODINFO_dbg_main=10
main_mod_runfunc 'run_mod_init $name $mod_dir stage3'
hgone_register_all_changes `system_trans_genid`
}
mkdir -p ./not-in-vcs/initgrid-2nd-stage/
run 2>&1 | tee ./not-in-vcs/initgrid-2nd-stage/newgrid.log

