#!/bin/bash

# functions for initgrid


install_zgridbase_hg2hg()
{
initgrid_hg_paths_default=`initgrid_hg_get_uplink_repo`
initgrid_hg_our_zgrid_path=`initgrid_get_our_zgrid_path`

if [ -z "$initgrid_hg_our_zgrid_path" ]; then
echo "initgrid_hg_our_zgrid_path == \"\" empty, aborting"
exit
fi
if [ -z "$initgrid_hg_paths_default" ]; then
echo "initgrid_hg_paths_default == \"\" empty"
else
echo "Use uplink initgrid_hg_paths_default as initgrid_hg_our_zgrid_path"
echo "initgrid_hg_our_zgrid_path=$initgrid_hg_paths_default"
initgrid_hg_our_zgrid_path=$initgrid_hg_paths_default
fi




pushd $zgridpath/$zgridname > /dev/null
#echo hg clone $initgrid_hg_paths_default
echo -n pwd=`pwd`
echo hg clone $initgrid_hg_our_zgrid_path zgrid
hg clone $initgrid_hg_our_zgrid_path zgrid
echo -n
popd > /dev/null
}


install_zgridbase_src_type_GET()
{
pushd $initgrid_BASEDIR_ZGRID  > /dev/null

if [ -d .hg/ ]; then
echo "hg"
return 0
fi
if [ -d .git/ ]; then
echo "git"
return 0
fi

echo "archive"
return 0
popd > /dev/null
}


initgrid_get_our_zgrid_path()
{
pushd $initgrid_BASEDIR_ZGRID > /dev/null
if [ -f ./.hg/store/00manifest.i ]; then
echo $initgrid_BASEDIR_ZGRID
fi
popd  > /dev/null
}


#####

zgridbase_export_archive()
{
echo "zgrid_base_export_archive()" 1>&2
cp -aR $initgrid_BASEDIR_ZGRID $dst
}


zgridbase_export_hg()
{
local dst=$1
echo "zgrid_base_export_hg() dst=$dst"
mkdir -p $dst
hg archive -t files $dst
}

zgridbase_export_git()
{
echo "zgrid_base_export_git()  dst=$dst"
nogit_warn;exit
}

zgridbase_export()
{
driverfunction2 zgridbase_export ${install_zgridbase_src_type} $*
}

###

install_zgridbase_createnew_hg()
{
echo -n

}
install_zgridbase_createnew_git()
{
nogit_warn;exit
}

install_zgridbase_createnew()
{
driverfunction2 install_zgridbase_createnew ${install_zgridbase_src_type} $*
}

################

nogit_warn()
{
echo "Currently git not supported, aborted"
}

## save install parameters

initgrid_save_install_params()
{
out="$initgrid_NEWDIR_ZGRID/not-in-vcs/"
mkdir $out
out=${out}/installed_grid.conf

cat /dev/null > ${out}
( set -o posix ; set )|grep ^install_ >> ${out}
( set -o posix ; set )|grep ^initgrid_ >> ${out}
}


################

mkdir_init()
{
echo "create $1"
mkdir -p ./$1
touch ./$1/.keep_me
}

################



