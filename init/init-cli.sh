#!/bin/bash


################## zgrid header1 ################ 
if [ x$ORIGDIR == x ]; then
export ORIGDIR=`pwd`
fi

_file0=`readlink -f $0`
cd `dirname $_file0`

. ./libzgrid.sh

################## [END] zgrid header1 ################ 

#main_exit_if_not_enabled init

echo 
echo "                  << init CLI util   >>"
echo 
#export MODINFO_dbg_init=4

run_mod_init2()
{
#echo mod_dir=$mod_dir
s="$mod_dir/${name}.initgrid"
#echo "$s"
if [ -f $s ]; then
echo -n
echo "[!] found $s"
else
echo "$name : .initgrid not found"
fi

}

main_mod_runfunc 'run_mod_init2 $mod_dir'



