#!/bin/bash


################## zgrid header1 ################ 
if [ x$ORIGDIR == x ]; then
export ORIGDIR=`pwd`
fi

_file0=`readlink -f $0`
initgrid_BASEDIR=`dirname $_file0`
cd $initgrid_BASEDIR
#. ./libzgrid.sh

pushd ../ > /dev/null
initgrid_BASEDIR_ZGRID=`pwd`
popd > /dev/null

################## [END] zgrid header1 ################ 

source $initgrid_BASEDIR/initgrid.bash
source $initgrid_BASEDIR/../modules/system/generic-code.bash

source $initgrid_BASEDIR/install_profile.conf

#######################################################

initgrid_hg_get_uplink_repo()
{
#cat ../.hg/hgrc
local var

var=`hg showconfig|grep paths.default` 1>&2
var=${var/paths./initgrid_hg_paths_}
#echo $var 1>&2
eval $var
echo $initgrid_hg_paths_default
}


actual_do_install()
{
echo "Begin actual_do_install()"
cd $initgrid_NEWDIR_ZGRID
echo "actual_do_install() : ./zgrid/init/initgrid-structure $zgridname"
./zgrid/init/initgrid-structure $zgridname
echo "End actual_do_install()"
}



#######################################################

echo 
echo "                  << initgrid util   >>"
echo 
#export MODINFO_dbg_init=4


export zgridname=$1
export zgridpath=$2
export zgridconffile=$3

#if [ x$zgridname == x ]; then
if [ x$zgridpath == x ]; then
echo "Usage: $0 <zgridname> <zgridpath> [<zgridconffile>]"
echo 
echo "   This script create new grid system in directory <zgridpath> with "
echo "   name <zgridname>. Additional parameters can be added in optional"
echo "   <zgridconffile> file in key-value format"
echo 
exit
fi

if [ ! -d $zgridpath ]; then
echo "$zgridpath not exists or not directory, exit"
exit
else
echo "Ok, $zgridpath is a directory, continue"
fi
if [ -w $zgridpath ]; then
echo "Ok, $zgridpath writable by you, continue"
else
echo "$zgridpath not writable, exit"
exit
fi

if [ x$zgridconffile != x ]; then
 if [ -f $zgridconffile ]; then
 echo "Ok, config file for installation found [$zgridconffile]"
 source $zgridconffile
 else
 echo "NOT Ok, config file for installation [$zgridconffile] not found, abort"
 echo
 exit
 fi
fi

if [[ $zgridname =~ ^[A-Za-z0-9]+$ ]]; then
echo "Ok, grid name is alphabet letters or digits"
else
echo "NOT Ok, grid name should be alphabet letters or digits"
exit
fi

echo 
echo initgrid_BASEDIR_ZGRID=$initgrid_BASEDIR_ZGRID
echo 
echo zgridname=$zgridname
echo zgridpath=$zgridpath
echo 

mkdir $zgridpath/$zgridname

export initgrid_NEWDIR_ZGRID=$zgridpath/$zgridname

echo initgrid_NEWDIR_ZGRID=$initgrid_NEWDIR_ZGRID

# install_zgrid_base_src_type_GET
export install_zgridbase_src_type=`install_zgridbase_src_type_GET`

echo install_zgridbase_src_type=$install_zgridbase_src_type [autodetect]
echo install_zgridbase_dst_type=$install_zgridbase_dst_type [config]
echo install_zgridbase_dst_createnew=$install_zgridbase_dst_createnew [config]


#exit
##


if [ x"$install_zgridbase_dst_createnew" == x0 ]; then
if [ x"$install_zgridbase_dst_type" == x"hg" ]; then
echo "run install_zgrid_base_hg2hg"
install_zgridbase_hg2hg
initgrid_save_install_params
actual_do_install
exit
fi;fi

# 



if [ x"$install_zgridbase_dst_createnew" == x1 ]; then
zgridbase_export ${initgrid_NEWDIR_ZGRID}/zgrid
install_zgridbase_createnew 
initgrid_save_install_params

actual_do_install
echo 
echo 
else
echo -n " install_zgridbase_dst_createnew == 0 , but not found correct install procedure"
fi


echo 
echo 
exit

