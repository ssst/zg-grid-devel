#!/bin/bash

devhelper_tname="template_one"

_devhelper_newmod_file()
{
while read FILE; do
FILENAME=`basename $FILE`
#${string/substring/replacement}
FILENAME=${FILENAME/.TEMPREMOVE/}
FILENAME=${FILENAME/TEMPLATE/$name}
echo "cat $FILE | sed s/TEMPLATE/$name/g > $dst/$FILENAME"
cat $FILE | sed s/TEMPLATE/$name/g > $dst/$FILENAME
if [ -x $FILE ]; then
chmod u+x,g+x,o+x $dst/$FILENAME
fi
done
}

devhelper_newmod()
{
name=$3
#name=

src="${MODINFO_modpath_devhelper}/${devhelper_tname}/"
dst="./zgrid-site/modules/$name"

#ls $src
#pwd;ls $dst
if [ -a $dst ]; then
echo "Abort - already exists!"
return
fi

echo ------ create $name module   --------------
echo "mkdir -p $dst"
mkdir -p $dst
find $src -type f | _devhelper_newmod_file
echo "cp zgrid/main/libzgrid/libzgrid.sh $dst/"
cp zgrid/main/libzgrid/libzgrid.sh $dst/
echo -------------------------------------------
}

####### hook ############

_devhelper_hook_list_var_hlp1()
{
find -iname \*.bash -exec egrep "^ *main_call_hook " \{\} \;
find -iname \*.sh -exec egrep "^ *main_call_hook " \{\} \;
#( find -iname *.bash -exec egrep "^ *main_call_hook " \{\} \; ; \
#find -iname *.bash -exec egrep "\`main_call_hook " \{\} \; ) | while read A B C ; 
#find -iname \*.bash -iname \*.sh -exec egrep "^ *main_call_hook " \{\} \; | sort | while read A B C ;
}

devhelper_hook_list_var()
{
cd $ZGRIDBASEDIR

_devhelper_hook_list_var_hlp1 | sort | while read A B C ;
do
#echo $A -- $B
echo -n $B " "
done
}
devhelper_hook_info()
{
local h v i
h=$1
v=`_main_call_hook_list $1`
for i in $v; do
echo -n "$i "
done

}

devhelper_hook_list()
{
local hooklistvar var
cd $ZGRIDBASEDIR

hooklistvar=`devhelper_hook_list_var`
echo " -- List all bashengine hooks -- "
for var in $hooklistvar ; do
echo hook_$var 
done
}

devhelper_hook_list_info()
{
local hooklistvar var hf
cd $ZGRIDBASEDIR

hooklistvar=`devhelper_hook_list_var|sort`
#echo " -- List all hooks -- "
for var in $hooklistvar ; do
hf="hook_$var"
printf "%18s" "hook name: "
printf "${hf}()"
echo
#echo "hook name: $hf"
printf "%18s" "hooked func's: "
devhelper_hook_info $var
#
echo 
echo ------------------
done
echo 
}
###### api #########

_devhelper_api_list_hlp1()
{
find -iname \*.bash -exec grep  "\[API\]" \{\} \;
find -iname \*.sh -exec grep "\[API\]" \{\} \;
}

devhelper_api_list()
{
cd $ZGRIDBASEDIR
echo ""
_devhelper_api_list_hlp1 | sort | sed "s/^function //"
echo ""
}

devhelper_grepcode()
{
local s
s=$1
cd $ZGRIDBASEDIR
echo ""
find -iname \*.bash -exec grep -n -H "$s" \{\} \;
find -iname \*.sh -exec grep -n -H "$s" \{\} \;
echo ""
}

devhelper_varlist()
{
set | grep "^ZGRID"
}
devhelper_vars()
{
zgridsys_vars_list
}


devhelper_callfunc()
{
#dbg_echo -n ""
f=$1
shift 1
argv=$*

if [ x${f} == "x"  ]; then
echo "empty funcname, exiting"
exit
fi

if is_function_exists $f ; then
$f $argv
else
echo "funcname \"$f\" not exists"
fi
}


############ cli integration  ################

devhelper_cli_help()
{
zgridsys_s;echo "devhelper newmod <name> <.... - create new module"
zgridsys_s;echo "devhelper hooklist - list hooks"
zgridsys_s;echo "devhelper hooklistinfo - list hooks with some info"
zgridsys_s;echo "devhelper apilist - api func list"
zgridsys_s;echo "devhelper apilist-generic - generic api func list"
zgridsys_s;echo "devhelper grepcode - grep str"
zgridsys_s;echo "devhelper varlist - list variables"
zgridsys_s;echo "devhelper vars - variables from modinfo sybsystem"
zgridsys_s;echo "devhelper callfunc [funcname] - call zgrid internal function"
#zgridsys_s;echo "devhelper scanhosts"
}


devhelper_cli_run()
{
maincmd=$1
cmd=$2
name=$3

dbg_echo devhelper 5  x${maincmd} == x"module"
if [ x${maincmd} == x"devhelper"  ]; then
echo -n
else
return
fi

if [ x${cmd} == x""  ]; then
echo -n
devhelper_cli_help
fi


if [ x${cmd} == x"newmod"  ]; then
echo -n
devhelper_newmod $*
fi

if [ x${cmd} == x"hooklist"  ]; then
echo -n
devhelper_hook_list $*
fi

if [ x${cmd} == x"hooklistinfo"  ]; then
echo -n
devhelper_hook_list_info $*
fi

if [ x${cmd} == x"apilist"  ]; then
echo -n
devhelper_api_list $*
fi


if [ x${cmd} == x"vars"  ]; then
echo -n
devhelper_vars $*
fi

if [ x${cmd} == x"apilist-generic"  ]; then
echo -n
devhelper_api_list $*| grep "\[GENERIC\]"
fi


if [ x${cmd} == x"grepcode"  ]; then
echo -n
shift 2
devhelper_grepcode $*
fi

if [ x${cmd} == x"varlist"  ]; then
echo -n
shift 2
devhelper_varlist $*
fi

if [ x${cmd} == x"callfunc"  ]; then
echo -n
shift 2
devhelper_callfunc $*
fi


}


