#!/bin/bash

#echo 0000

nodecfg_gethostlist_hlpr()
{
echo "$HOST_id : hst=$HOST_dnsname"
}
nodecfg_gethostlist()
{
hostcfg_iterate_hostid 'nodecfg_gethostlist_hlpr'
}

_nodecfg_nodelist_col1()
{
var=$1
_name=$2
if [ x"${!var}" == "x" ]; then echo -n ;
else echo -n ${_name}=\"${!var}\"; 
echo -n " "
fi
}

_nodecfg_nodelist_col2()
{
var=$1
_name=$2
if [ x"${!var}" == "x" ]; then echo -n ;
else 
echo  "       "${_name}=\"${!var}\"
fi
}


_nodecfg_getnodelist_short_hlpr()
{
printf "%20s : " $NODE_ID
##_nodecfg_nodelist_col1 NODE_hostname hostname;
_nodecfg_nodelist_col1 HOST_dnsname dns;
_nodecfg_nodelist_col1 NODE_INSTPATH path;
##echo -n "hostname=$NODE_hostname "
##echo -n "grp=\"$NODE_GROUPS_append\" "
#_nodecfg_nodelist_col1 NODE_GROUPS_append grp;
#_nodecfg_nodelist_col1 NODE_INSTPATH inst;
#echo ----
echo
}

nodecfg_getnodelist_short()
{
nodecfg_iterate_full_nodeid _nodecfg_getnodelist_short_hlpr
}



_nodecfg_getnodelist_full_hlpr()
{
local var
#printf "== %25s  " $NODE_ID
echo " ==  $NODE_ID   [$NODE_HOST]"
echo
for var in `nodeid_vars_all` ; do 
  if [ -n "${!var}" ]; then
    echo $var=${!var}
  else
    echo "$var [NOT SET]"
  fi

done



echo
echo
}

nodecfg_getnodelist_full()
{
nodecfg_iterate_full_nodeid _nodecfg_getnodelist_full_hlpr
}

print_vars()
{
local $i
for i in $*; do
if [ -n "${!i}" ]; then
echo $i=${!i}
fi
done
}


source ${MODINFO_modpath_zgridsys}/thishostinfo.inc.bash

zgridsys_nodecfg_addthis()
{
HOST_dnsname=$1

if [ -n "$HOST_dnsname" ]; then
NODE_HOST=${HOST_dnsname%%.*}
HOST_id=${HOST_dnsname%%.*}
else
NODE_HOST=`hostname -s`
HOST_id=`hostname -s`
HOST_dnsname=`hostname -f`
fi

if [ -n "$NODE_ID" ]; then
echo -n
else
NODE_IDsuffix=`this_node_idsuffix`
NODE_ID="${USER}@${HOST_id}:$NODE_IDsuffix"
fi
echo NODE_ID=$NODE_ID

#hostcfg_hostid_exists "${HOST_id}"
#ret=$? #echo "hostid=${HOST_id}  ret=$ret"
#if [ "x$ret" == "x0"  ];  then
if hostcfg_hostid_exists "${HOST_id}" ;  then
echo "hostcfg_hostid_exists "${HOST_id}" - host id already exists"
hostid_exists="1"
fi
if nodecfg_nodeid_exists "${NODE_ID}" ;  then
echo "nodecfg_nodeid_exists "${NODE_ID}" - node id already exists"
nodeid_exists="1"
fi

local var=MODINFO_dbg_zgridsys
if [ ${!var} -le 4 ]; then
echo "-- Vars: --"
set | grep "^ZGRID"
#zgridsys_vars_list
echo "-- end Vars: --"
fi

outdir_host=${ZGRID_dir_nodelocal}/attach/thisnode/cfg/${HOST_id}/
outfile_host=${outdir_host}/${HOST_id}.hostinfo

outdir_node=${ZGRID_dir_nodelocal}/attach/thisnode/cfg/${NODE_ID}/
outfile_node=${outdir_node}/this.nodeconf

mkdir_ifnot ${outdir_host}
mkdir_ifnot ${outdir_node}


if [ x$hostid_exists != "x1" ]; then

echo
echo "# -- this_hostinfo -- "
this_hostinfo
echo -n "hostid_vars_all=" ; hostid_vars_all
print_vars `hostid_vars_all`
print_vars `hostid_vars_all` > $outfile_host

fi # if [ $hostid_exists != "1" ]; then


if [ x$nodeid_exists != "x1" ]; then

echo
echo "# -- this_nodeinfo -- "
this_nodeinfo
print_vars `nodeid_vars_all` | grep -v "^HOST_"
print_vars `nodeid_vars_all` | grep -v "^HOST_" > $outfile_node
echo

fi # 

}


zgridsys_cli_help_nodecfg()
{
#echo -n
zgridsys_s;echo "nodecfg hostlist - list modules"
zgridsys_s;echo "nodecfg hostadd <> - ..."
zgridsys_s;echo "nodecfg add <> - ..."
zgridsys_s;echo "nodecfg addthis - try to add host & node of this install"
zgridsys_s;echo "nodecfg nodelist <> - ..."
zgridsys_s;echo "nodecfg nodelist-full <> - ..."

}

_add_hostcfg_hlpr()
{
#echo [2] hst1=$hst1 1>&2 
#echo [2] hst2=$hst2 1>&2 
echo [2] HOST_NAME=$HOST_NAME 1>&2 
}

zgridsys_nodecfg_add_hostcfg()
{
#export hst1=$1 
#hst2=$2

HOST_NAME=$1
HOST_hostname=$2

nodecfg_add_hostcfg _add_hostcfg_hlpr
}


zgridsys_cli_nodecfg()
{
hst1=$3
param1=$3
hst2=$4


if [ x$cmd == x"hostlist" ]; then
#echo
echo "--- host of nodes list  ---"
nodecfg_gethostlist
echo
return
fi

if [ x$cmd == x"nodelist" ]; then
echo
echo                  --------- node list -----------
echo
nodecfg_getnodelist_short
echo
return
fi

if [ x$cmd == x"nodelist-full" ]; then
#echo
echo                  --------- node list -----------
#echo
nodecfg_getnodelist_full
echo
return
fi


#echo 11111
if [ x$cmd == x"addthis"  ]; then
dbg_echo nodecfg_cli 3 "do addthis"
shift 2
zgridsys_nodecfg_addthis $*
return
fi


if [ x$hst1 == x  ]; then
zgridsys_cli_help_nodecfg
zgridsys_cli_help_cfgfiles
#echo ".... module <name>"
exit
fi


dbg_echo nodecfg_cli 4 "Before hostadd"

if [ x$cmd == x"hostadd"  ]; then
dbg_echo nodecfg_cli 3 "do hostadd"

if [ x$hst2 == x  ]; then
echo "nodecfg hostadd  <host name as ID,short> <hostname>"
echo 
exit
fi
#nodecfg_add_hostcfg_cli1 $hst1 $hst2
zgridsys_nodecfg_add_hostcfg $hst1 $hst2
exit
fi
}