#!/bin/bash

#echo 0000

zgridsys_cli_help_module()
{
#echo -n
#echo "zgridsys_cli_main_help(): TEST"
zgridsys_s;echo "module list - list modules"
zgridsys_s;echo "module vars"
zgridsys_s;echo "module cache_clear - clear all cache"
zgridsys_s;echo "module enable [module name] - ..."
zgridsys_s;echo "module disable [module name] - ..."
zgridsys_s;echo "module activate [module name] - activate module on this node"
zgridsys_s;echo "module print-all-info - info from all modules printed by defined hook"

}

zgridsys_vars_list()
{
set|grep ^MODINFO_
set|grep ^MODULE_
}

zgridsys_module_print_all_info()
{
main_call_hook print_module_info
}

zgridsys_cli_module()
{
cmd=$2
name=$3


if [ x$cmd == x"list" ]; then
echo
main_getmodlist
echo
return
fi

if [ x$cmd == x"vars" ]; then
echo
zgridsys_vars_list
echo
return
fi

if [ x$cmd == x"print-all-info" ]; then
echo
zgridsys_module_print_all_info
echo
return
fi



if [ x$cmd == x"cache_clear" ]; then
echo
#zgridsys_vars_list
cache_clear ALL
echo
return
fi




if [ x$name == x  ]; then
zgridsys_cli_help_module
#echo ".... module <name>"
exit
fi


if [ x$cmd == x"enable" ]; then
#echo "Run main_enable_one_mod $name"
system_enable_one_mod $name
fi

if [ x$cmd == x"disable" ]; then
#echo "Run main_enable_one_mod $name"
system_disable_one_mod $name
fi

if [ x$cmd == x"activate" ]; then
#echo "Run main_enable_one_mod $name"
system_activate_one_mod $name
fi


}
