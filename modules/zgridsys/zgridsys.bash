#!/bin/bash

if [ x$MODINFO_loaded_zgridsys == "x" ]; then
export MODINFO_loaded_zgridsys="Y"
else
return
fi

#export MODINFO_dbg_zgridsys=4

#ZGRIDSYS_context=service|client|unknown

zgridsys_print_module_info()
{
echo "zgridsys: mod info"
}

zgridsys_activate_on_this_node()
{
cmd1=$HOME/bin/${ZGRID_zgridname}-zgridsys
if [ -f $cmd1 ]; then
echo "ok, \$HOME/bin/${ZGRID_zgridname}-zgridsys already installed"
else
set -x
#$ZGRID_zgridname
mkdir_ifnot $HOME/bin/
ln -s $ZGRIDBASEDIR/zgrid-site/bin/${ZGRID_zgridname}-zgridsys $HOME/bin/
echo -n
set +x
fi

}


zgridsys_s()
{
echo -n "     "
}

source ${MODINFO_modpath_zgridsys}/module.inc.sh
source ${MODINFO_modpath_zgridsys}/nodecfg.inc.sh
source ${MODINFO_modpath_zgridsys}/cfgfiles.inc.sh
source ${MODINFO_modpath_zgridsys}/status.inc.sh


zgridsys_f()  # [API] [RECOMENDED]
{
#echo -n
#pushd $ZGRIDBASEDIR > /dev/null
#./zgrid/modules/zgridsys/zgridsys $*
#popd > /dev/null
zgridsys_f_cleanenv $*
}


zgridsys_f_cleanenv()  # [API] [RECOMENDED]
{
pushd $ZGRIDBASEDIR > /dev/null
#( set -o posix ; set ) #exit
system_f_cleanenv ./zgrid/modules/zgridsys/zgridsys $*
popd > /dev/null
}

zgridsys_cli_help_helpsys()
{
zgridsys_s;echo "help <topic> - call help system on topic"
}

zgridsys_cli_help()
{
zgridsys_cli_help_helpsys
zgridsys_cli_help_module
zgridsys_cli_help_nodecfg
zgridsys_cli_help_cfgfiles
zgridsys_cli_help_status

zgridsys_s;echo "exec <cmd>  - execute shell cmd"
}



zgridsys_cli_main_help()
{
cli_name=`basename $0`
echo " "
echo "Usage: $cli_name  <sub cmd name> .. <param1> ..."
echo " "
}

zgridsys_cli_help_hook()
{
zgridsys_cli_main_help
main_call_hook cli_help $*
}

export zgridsys_cli_run_argv
zgridsys_cli_run_hook()
{
local var
#zgridsys_cli_main_run
export zgridsys_cli_run_argv=$*
var=$*
var=${var//(/\\\(}
var=${var//)/\\\)}
var=${var//;/\\\;}
#if thenfi #echo var=$var
main_call_hook cli_run  $var
}


zgridsys_cli_run()
{
maincmd=$1
cmd=$2
name=$3
#echo $*
#exit

dbg_echo zgridsys 5  x${maincmd} == x"module"
if [ x${maincmd} == x"module"  ]; then
zgridsys_cli_module $*
return
fi

dbg_echo zgridsys 5 x${maincmd} == x"module"
if [ x${maincmd} == x"help"  ]; then
zgridsys_cli_helpsys $*
return
fi

if [ x${maincmd} == x"nodecfg"  ]; then
zgridsys_cli_nodecfg $*
zgridsys_cli_cfgfiles $*
return
fi


if [ x${maincmd} == x"status"  ]; then
zgridsys_cli_status $*
return
fi

###### misc ########

if [ x${maincmd} == x"exec"  ]; then
shift 1
eval $*
return
fi


}

zgridsys_cli_helpsys()
{
echo "helpsys stub"
}


zgridsys_cli_main() # [API] [RECOMENDED]
{
#echo "cli start"
cmd=$1

if [ x"$cmd" == x -o x"$cmd" == x"NONE" ]; then
zgridsys_cli_help_hook $*
echo ""
exit
fi
zgridsys_cli_run_hook $*
#echo "cmd \"$1\" not found"
}
