#!/bin/bash

function system_config_samples_install_item
{
local mod=${ARRAY[0]}

#echo ${ARRAY[1]}
#echo ${ARRAY[2]}
#echo ${ARRAY[3]}
local varname="MODINFO_modpath_${mod}"
#echo $varname#echo aaaa=$MODINFO_modpath_updmod1
eval "moddir=\$$varname"
#moddir=${!varname}#echo bbb=${!varname}#echo moddir=\"$moddir\";

local out=${system_zgrid_sample_node_dir}/${ARRAY[3]}
local outdir=`zg_dirname $out`
echo mkdir_ifnot $outdir

echo "cp ${moddir}/${ARRAY[2]} ./${out}"
}


###########

function system_installable_files_install_op_bin
{
#local out=${system_zgrid_sample_node_dir}/${outfile}
local out=./zgrid-site/bin/${outfile}
local outdir=`zg_dirname $out`
echo mkdir_ifnot $outdir

echo "cp ${moddir}/${infile} ${out}"

system_trans_register  $CURRENT_TRANSACTION_ID system installable_files_install ${out} op_bin 

#local out=${system_zgrid_sample_node_dir}/${outfile}
}

function system_installable_files_install_op_config_sample_node
{
local out=${system_zgrid_sample_node_dir}/${outfile}
local outdir=`zg_dirname $out`
echo mkdir_ifnot_q $outdir
mkdir_ifnot_q $outdir

echo "cp ${moddir}/${infile} ${out}"
cp ${moddir}/${infile} ${out}

system_trans_register  $CURRENT_TRANSACTION_ID system installable_files_install ${out} op_bin 
}

function system_installable_files_install_op_config_default_allgrid
{
local out=./zgrid-site/bin/${outfile}
local outdir=`zg_dirname $out`
echo mkdir_ifnot $outdir

echo "cp ${moddir}/${infile} ${out}"
}


######

function system_installable_files_install_item
{
local mod=${ARRAY[0]}
local op=${ARRAY[1]}
local flags=${ARRAY[2]}

local infile=${ARRAY[3]}
local outfile=${ARRAY[4]}

local varname="MODINFO_modpath_${mod}"
#echo $varname#echo aaaa=$MODINFO_modpath_updmod1
eval "moddir=\$$varname"
#moddir=${!varname}#echo bbb=${!varname}#echo moddir=\"$moddir\";


local F_op="system_installable_files_install_op_${op}"

if is_function_exists $F_op ; then
echo "---- Run operation \"${op}\" ----"
$F_op
echo "---- End operation \"${op}\" ----"
else
echo "No such operation \"${op}\""
fi



}
function system_installable_files_get
{
echo -n
main_call_hook installable_files $*
}


function system_installable_files_install
{
local mod=$1
local trid=$2

if  [ x$mod == x ]; then echo "mod!!";return; fi

echo "system_installable_files_install: begin"
#echo --------------
#system_installable_files_get
#echo --------------
local cfgstr
system_installable_files_get|grep ^$mod| while read cfgstr; do
#echo "cfgstr=$cfgstr --"
echo $cfgstr
split_var system_installable_files_install_item  " : " $cfgstr
done

echo "system_installable_files_install: end"
}

function system_installable_files_remove
{
local mod=$1

}

