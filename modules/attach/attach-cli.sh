#!/bin/bash


################## zgrid header1 ################ 
if [ x$ORIGDIR == x ]; then
export ORIGDIR=`pwd`
fi

_file0=`readlink -f $0`
cd `dirname $_file0`

. ./libzgrid.sh

################## [END] zgrid header1 ################ 

main_exit_if_not_enabled attach

echo 
echo "                  << attach CLI util   >>"
echo 

if [ x$1 == x ]; then
echo "$0 <path-to-node> [-n <id>] "
echo "   attach newnode in same user on same host in directory <path>"
exit
fi

#export MODINFO_dbg_attach=4
export MODINFO_dbg_zgridsys=4

#set -x
attach_newnode_local $*

