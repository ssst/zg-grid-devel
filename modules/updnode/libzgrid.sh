
################## zgrid support lib for CLI util header ####################
####  You should place this file to zgrid module directory   ################
#############################################################################

export ZGRIDLIB_version="0.1-0.5"

checkpath="../../ ../../../ ../../../../ ../../../../../"

export ZGRIDLIB_thisclidir=`pwd`

for d in $checkpath; do

pushd $d > /dev/null
#pwd
if [ -f ./zgrid/main/loadconfigs.sh ]; then
#echo OK ./zgrid/main/loadconfigs.sh
. ./zgrid/main/loadconfigs.sh

unset checkpath;
return
else
echo -n
fi
popd > /dev/null
done
echo "zgrid installation not found, aborting."
exit

################################################
