#!/bin/bash

testmod_print_module_info()
{
echo "testmod: mod info"

}

#export _testmod_full_srv_implementation=1
export _testmod_full_srv_implementation=0

testmod_full_srv_implementation()
{
if [ x${_testmod_full_srv_implementation} == x1 ]; then
# uncomment to enable
echo "testmod_full_srv_implementation(): implemented"
export srv_full_srv_implementation=1
fi
return
}
# hook_ srv_is_registered_service

testmod_srv_is_registered_service()
{
echo testmod_daemon
}


testmod_status_service_testmod_daemon()
{
echo -n testmod_daemon:STATUS
}

testmod_start_service_testmod_daemon()
{
echo testmod_daemon START
}

testmod_stop_service_testmod_daemon()
{
echo testmod_daemon STOP
}


testmod_help_service_testmod_daemon()
{
echo testmod_daemon HELP
}


