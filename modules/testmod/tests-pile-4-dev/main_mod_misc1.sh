#!/bin/bash


################## zgrid header1 ################ 
if [ x$ORIGDIR == x ]; then
export ORIGDIR=`pwd`
fi

_file0=`readlink -f $0`
_dir0=`dirname $_file0`
cd ${_dir0}

. ./libzgrid.sh
################## [END] zgrid header1 ################ 

source zgrid/modules/testmod/testmod_lib.bash

#main_exit_if_not_enabled zgridsys

#export MODINFO_dbg_main=12
#local ret

main_check_mod_present zgridsys
ret=$?
testmod_test_print_str "main_check_mod_present zgridsys" 0 $ret


echo
echo main_check_mod_present zgridsys
main_check_mod_present zgridsys
ret=$?
echo -n "normal ret=0 : "
echo -n "ret=$ret"
echo -n " |   "
testmod_check_test_str $ret 0
echo

echo
echo main_check_mod_present zgridsysaaaa
main_check_mod_present zgridsysaaaa
ret=$?
echo -n "normal ret=255 : "
echo -n " ret=$ret"
echo -n " |   "
testmod_check_test_str $ret 255
echo

#zgridsys_cli_main "$*"
