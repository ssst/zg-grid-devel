#!/bin/bash

testmod_check_test_str()
{
if [ x$1 == x$2 ]; then
echo -n "[passed]"
else
echo -n " !!!!!! ERROR  !!!!!!"
fi
}
testmod_test_print_str()
{
local ourtest=$1
local norm_value=$2
local tested_value=$3
local ret=$tested_value
echo
echo $1
echo -n "normal=$norm_value : "
echo -n "tested=$ret"
echo -n " |  "
testmod_check_test_str $ret $norm_value
echo
}

