#!/bin/bash

################## zgrid header1 ################ 
if [ x$ORIGDIR == x ]; then
export ORIGDIR=`pwd`
fi

_file0=`readlink -f $0`
_dir0=`dirname $_file0`
cd ${_dir0}

. ./libzgrid.sh
################## [END] zgrid header1 ################ 

main_exit_if_not_enabled testmod

echo 
echo "                  << testmod.sh CLI util   >>"
echo 

#export MODINFO_dbg_main=3
main_call_hook print_module_info
