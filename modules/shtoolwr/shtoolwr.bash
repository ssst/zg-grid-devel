#!/bin/bash

if [ x$MODINFO_loaded_shtoolwr == "x" ]; then
export MODINFO_loaded_shtoolwr="Y"
else
return
fi


shtoolwr_check_path()
{
cfg=$1
#echo ZGRIDBASEDIR=$ZGRIDBASEDIR 1>&2
#echo GRIDBASEDIR=$GRIDBASEDIR 1>&2
if [ -f $cfg ]; then
echo "export SHTOOLWR_shtool_path=\"$cfg\" ;"
else
dbg_echo shtoolwr 3 echo $cfg not found 2>&1
fi
}

shtoolwr_envset_start()
{
pushd $ZGRIDBASEDIR > /dev/null
echo "export SHTOOLWR_shtool_path=\"echo\" ;"
shtoolwr_check_path ${MODINFO_modpath_shtoolwr}/library/shtool
shtoolwr_check_path ${MODINFO_modpath_shtoolwr}/libraries/shtool
shtoolwr_check_path ./zgrid-site/libraries/shtool/shtool
popd > /dev/null
}

# load other support modules

shtoolwr_shtool()
{
pushd $ZGRIDBASEDIR > /dev/null
#ls $MODINFO_modpath_shtoolwr
echo "run: shtool $*"
${SHTOOLWR_shtool_path} $*
#${MODINFO_modpath_shtoolwr}/library/shtool $*
popd > /dev/null
}

shtoolwr_osstring()
{
pushd $ZGRIDBASEDIR > /dev/null
#shtoolwr_shtool platform -v -F "\"%sc (%ac) %st (%at) %sp (%ap)\""
#${MODINFO_modpath_shtoolwr}/library/shtool platform -v -F "%sc (%ac) %st (%at) %sp (%ap)"
${SHTOOLWR_shtool_path}  platform -v -F "%sc (%ac) %st (%at) %sp (%ap)"
popd > /dev/null
}

shtoolwr_cli_run()
{
maincmd=$1
cmd=$2

if [ x${maincmd} == x"util" -a  x${cmd} == x"shtool"  ]; then
shift 2
shtoolwr_shtool $*
echo -n
#else
#return
fi

if [ x${maincmd} == x"util" -a x${cmd} == x"osstring"  ]; then
shtoolwr_osstring
echo -n
#else
#return
fi

}

#shtoolwr_cli_run()


shtoolwr_cli_help()
{
zgridsys_s;echo "util shtool - shtool utility wrapper"
zgridsys_s;echo "util osstring - OS identification string get from shtool"

}
