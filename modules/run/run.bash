#!/bin/bash

if [ x$MODINFO_loaded_run == "x" ]; then
export MODINFO_loaded_run="Y"
else
return
fi

MODINFO_dbg_run=0
#MODINFO_enable_run=

run_hostid_vars()
{
echo -n " CONNECT_dnsname "
}

run_env_start()
{
RUN_SSH_OPTS=" -A "
#RUN_SSH_CMD="ssh"
RUN_SSH_CMD=`a_cmd=ssh a_mod=run system_alternative`
dbg_echo run 3 "RUN_SSH_CMD=$RUN_SSH_CMD" 1>&2
#RUN_SSH_OPTS=$RUN_SSH_OPTS
}


run_hostid_post_load_api()
{
run_connect_dnsname_set
#echo "run_hostid_post_load_api , CONNECT_dnsname=$CONNECT_dnsname " 1>&2
}


run_connect_dnsname_set()
{
local name
local _hostid=$1

#if [ x"$MODINFO_enable_hoststat" == xY  ]; then
#fi

#HOST_dnsname
#RUN_dnsname
#CONNECT_dnsname

if [ -n "$incoming_scanhst"  ]; then
CONNECT_dnsname=$incoming_scanhst
fi

if [ -z "$CONNECT_dnsname" -a -n "$HOST_dnsname"   ]; then
CONNECT_dnsname=$HOST_dnsname
fi

}


run_print_module_info()
{
echo "run: mod info, called run_print_module_info"

}

_is_nodeid_exists()
{
echo -n
}


run_nodecmd()
{
local zgridsyscmd="./zgrid/modules/zgridsys/zgridsys"
#local r_cmd="run local_exec"
local nodeid cmd params r_cmd

if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi


nodeid=$1
#cmd=$2
#shift 3
shift 1
params=$*

if [ x"$params" == x ]; then
echo "command to execute on remote host not set"
echo "rus as: zgridsys run nodecmd [cmd] [param1] [param2] ..."
#zgridsys_f_cleanenv
exit
fi

if [ x"$cli_cmd" == x"nodeshellcmd" ]; then
r_cmd="run local_exec"
fi


dbg_echo run 3 "run_nodecmd(): this=$THIS_NODEID"
pushd $ZGRIDBASEDIR > /dev/null

nodecfg_nodeid_load $nodeid
#exit
if [ "x$?" == "x0" ]; then
echo -n
else
echo "abort ret=$?"
exit
fi
#echo "nodeid=$NODE_ID" # echo "NODE_HOST=$NODE_HOST" #echo "HOST_dnsname=$HOST_dnsname" #echo NODE_USER=$NODE_USER
  if [ x == x$NODE_USER ]; then
   echo -n
  else
   CONNECT_dnsname=${NODE_USER}@${CONNECT_dnsname}
  fi

code="$RUN_SSH_CMD $RUN_SSH_OPTS $CONNECT_dnsname \"(cd $NODE_INSTPATH ; $zgridsyscmd $r_cmd ${params} )\""
dbg_echo run 2 "run_nodecmd():" $code
$RUN_SSH_CMD $RUN_SSH_OPTS  $CONNECT_dnsname "(cd $NODE_INSTPATH ; $zgridsyscmd $r_cmd ${params} )"

#$code
#$RUN_SSH_CMD $RUN_SSH_OPTS  $CONNECT_dnsname "(cd $NODE_INSTPATH ; $zgridsyscmd $r_cmd ${params})"

popd > /dev/null
}

run_nodecli()
{
local zgridsyscmd="./zgrid/modules/zgridsys/zgridsys"
local nodeid cmd params r_cmd

if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi


nodeid=$1
shift 1
params=$*

if [ x"$params" == x ]; then echo "command to execute on remote host not set"; exit ; fi

pushd $ZGRIDBASEDIR > /dev/null

nodecfg_nodeid_load $nodeid
if [ "x$?" == "x0" ]; then
echo -n
else
echo "abort ret=$?"
exit
fi
#echo "nodeid=$NODE_ID" # echo "NODE_HOST=$NODE_HOST" #echo "HOST_dnsname=$HOST_dnsname" #echo NODE_USER=$NODE_USER
  if [ x == x$NODE_USER ]; then
   echo -n
  else
   CONNECT_dnsname=${NODE_USER}@${CONNECT_dnsname}
  fi

code="$RUN_SSH_CMD $RUN_SSH_OPTS $CONNECT_dnsname \"(cd $NODE_INSTPATH ; $r_cmd ${params} )\""
dbg_echo run 2 "run_nodecmd():" $code
$RUN_SSH_CMD $RUN_SSH_OPTS  $CONNECT_dnsname "(cd $NODE_INSTPATH ; $r_cmd ${params} )"

#$code
#$RUN_SSH_CMD $RUN_SSH_OPTS  $CONNECT_dnsname "(cd $NODE_INSTPATH ; $zgridsyscmd $r_cmd ${params})"

popd > /dev/null

}

run_hostcmd()
{
local nodeid cmd params r_cmd
_hostid=$1

if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi
dbg_echo run 2 "run_hostcmd() params=$*"

shift 1


export hostinfo_loadconf_mode_exports=1
varX=`hostcfg_hostid_load ${_hostid}`
eval $varX;unset varX

if [ -z "$HOST_id" ]; then
CONNECT_dnsname="${_hostid}"
fi

#code="ssh $HOST_dnsname $*"
#code="ssh $CONNECT_dnsname $*"
code="$RUN_SSH_CMD $CONNECT_dnsname $*"

dbg_echo run 1 "code=\"$code\"" 1>&2
$code

}


run_local_exec()
{
#shift 1
echo "hostname=$HOSTNAME , Executing: " "$*"
echo -n "pwd=" ;pwd
#eval $*
zgridsys_f_cleanenv $*
}

############ grid level runs   ################


_run_allgrid_nodecmd_hlpr()
{
#echo TTT=$*
if [ x$hoststat_isup_this_host == "x1" ]; then
#echo "Node ${NODE_ID} online, run cmd"
dbg_echo run 4 "run_nodecmd ${NODE_ID} ${_run_params}"
run_nodecmd ${NODE_ID} ${_run_params}
else
dbg_echo run 1 "Node \"${NODE_ID}\" offline, do nothing"
msg_echo run 2 "Node \"${NODE_ID}\" offline, do nothing"
fi

}

run_allgrid_nodecmd()
{
echo -n
export _run_params=$*
nodecfg_iterate_full_nodeid _run_allgrid_nodecmd_hlpr $*
}

run_allgrid_nodecmd_cmd()
{
run_allgrid_nodecmd $*
}

###

_run_allgrid_hostcmd_hlpr()
{
#echo TTT=$*
if [ x$hoststat_isup_this_host == "x1" ]; then
#echo "Node ${NODE_ID} online, run cmd"
dbg_echo run 4 "run_nodecmd ${NODE_ID} ${_run_params}"
run_hostcmd ${HOST_id} ${_run_params}
else
msg_echo run 2 "Host \"${HOST_id}\" offline, do nothing"
dbg_echo run 1 "Host \"${HOST_id}\" offline, do nothing"
fi

}

run_allgrid_hostcmd()
{
echo -n
if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi
export _run_params=$*
dbg_echo run 2 "run_allgrid_hostcmd() : params=\"$*\""
hostcfg_iterate_hostid _run_allgrid_hostcmd_hlpr $*
}

run_allgrid_hostcmd_cmd()
{
if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi
export _run_params=$*
run_allgrid_hostcmd $*
}

###

_run_allgrid_nodeshellcmd_hlpr()
{
#echo TTT=$*
if [ x$hoststat_isup_this_host == "x1" ]; then
#echo "Node ${NODE_ID} online, run cmd"
dbg_echo run 4 "run_nodecmd ${NODE_ID} ${_run_params}"
run_hostcmd ${HOST_id} ${_run_params}
else
msg_echo run 2 "Host \"${HOST_id}\" offline, do nothing"
dbg_echo run 1 "Host \"${HOST_id}\" offline, do nothing"
fi

}

run_allgrid_nodeshellcmd()
{
echo -n
if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi
export _run_params=$*
dbg_echo run 2 "run_allgrid_nodeshellcmd() : params=\"$*\""
hostcfg_iterate_hostid _run_allgrid_nodeshellcmd_hlpr $*
}

run_allgrid_nodeshellcmd_cmd()
{
if [ "x$*" == "x" ]; then
echo "run: need parameters"
exit
fi
export _run_params=$*
run_allgrid_nodeshellcmd $*
}



############ cli integration  ################

run_cli_help()
{
zgridsys_s;echo "run nodecmd <node id> <cmd> - run zgridsys command on node <nodeid>"
zgridsys_s;echo "run hostcmd <host id> <cmd> - run command on host <hostid>"
zgridsys_s;echo "run local_exec - helper func for run"
zgridsys_s;echo "run nodeshellcmd <node id> <cmd> - run shell command on node <nodeid>"
zgridsys_s;echo "run nodecli <node id> [cmd] - run cli command on node <nodeid> like ../module-list"

zgridsys_s;echo "run allgrid-nodecmd <cmd> - run zgridsys command on all (active) nodes"
zgridsys_s;echo "run allgrid-hostcmd <cmd> - run command on all (active) hosts"
zgridsys_s;echo "run allgrid-nodeshellcmd <cmd> - run shell command on   on all (active) hosts"
}


run_cli_run()
{
maincmd=$1
cmd=$2
name=$3

dbg_echo run 2 run_cli_run zgridsys_cli_run_argv=$zgridsys_cli_run_argv
#*=$zgridsys_cli_run_argv
#exit

dbg_echo run 5  x${maincmd} == x"run"
if [ x${maincmd} == x"run"  ]; then
echo -n
else
return
fi

if [ x${cmd} == x""  ]; then
echo -n
run_cli_help
fi

if [ x${cmd} == x"nodeshellcmd"  ]; then
echo -n
shift 2
cli_cmd=$cmd run_nodecmd $*
fi


if [ x${cmd} == x"nodecmd"  ]; then
echo -n
shift 2
run_nodecmd $*
fi





if [ x${cmd} == x"nodecli"  ]; then
echo -n
shift 2
run_nodecli $*
fi

if [ x${cmd} == x"hostcmd"  ]; then
echo -n
shift 2
run_hostcmd $*
fi


if [ x${cmd} == x"local_exec"  ]; then
echo -n
shift 2
run_local_exec $*
fi


if [ x${cmd} == x"allgrid-nodecmd"  ]; then
echo -n
shift 2
run_allgrid_nodecmd_cmd $*
fi

if [ x${cmd} == x"allgrid-hostcmd"  ]; then
echo -n
shift 2
run_allgrid_hostcmd_cmd $*
fi

if [ x${cmd} == x"allgrid-nodeshellcmd"  ]; then
echo -n
shift 2
run_allgrid_nodeshellcmd_cmd $*
fi



}

